# Assignment Angular-pokemon-trainer

# Link Heroku: https://noroff-heroku-pokemon-com-on2.herokuapp.com/

## Table of Contants
- [Install](#install)
- [Usage](#usage)
- [Description](#description)
- [Maintainers](#maintainers)
- [Visuals](#visuals)
- [Project status](#project-status)

## Install

##### Clone ripository from git

    git clone https://gitlab.com/EsbenLB/angular-pokemon-trainer.git

##### Then install

    npm install
    npm install he

## Usage

    ng serve

## Description
Login, collected pokemons, display pokemons and remove pokemon

### LoginScreen
Login with trainer name.

### CatalogueScreen
Add pokemons to your collecting

### TrainerScreeen
Remove pokemon from your collecting

## Maintainers
Esben Bjarnason - @EsbenLB

Erlend Hollund - @Holdude


## Visuals

### Component tree:

![Component tree](Untitled_Figma.pdf)


## Project status

## Not done
Display 200 pokemons instead of 1118  
No logout button  
When deleting pokemon from inventory, page does not update itself  
