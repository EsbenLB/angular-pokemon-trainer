import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { PokemonloginComponent } from './pokemon-login/pokemon-login.component';
import { PokemonCataloguePage } from './pokemon-catalogue/pokemon-catalogue.page';
import { TrainerPage } from './trainer/trainer.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login',
  },
  {
    path: 'login',
    component: PokemonloginComponent
  },
  {
    path: 'trainer',
    component: TrainerPage,
    canActivate: [AuthGuard]
  },
  {
    path: 'catalogue',
    component: PokemonCataloguePage,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
