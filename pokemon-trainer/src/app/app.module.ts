import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonCataloguePage } from './pokemon-catalogue/pokemon-catalogue.page';
import { TrainerPage } from './trainer/trainer.page';
import { FormsModule } from '@angular/forms';
import { PokemonloginComponent } from './pokemon-login/pokemon-login.component';

@NgModule({ // Decorator
  declarations: [ // new components in here
    AppComponent,
    PokemonCataloguePage,
    TrainerPage,
    PokemonloginComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,

    //PokemonsPage, // tutorial have this line here?
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
