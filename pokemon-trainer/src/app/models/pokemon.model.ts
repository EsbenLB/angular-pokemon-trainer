
// interface

export interface User {
    id: number;
    username: string;
    pokemon: [];
}

export interface Pokemon {
    id:        number;
    name:      string;
  }