export interface Pokemons {
    count:    number; // number of pokemon's
    next:     string; // url for next "page" of pokemon
    previous: string; // url for prev "page" of pokemon
    results:  Result[]; // all pokemon's
}

export interface Result {
    name: string; // pokemon name
    url:  string; // other info about pokemon
    pic: string;
    isCollected: boolean;
    pokemon:  Pokemon[]; // all pokemon's

}

export interface Pokemon {
  id:        number; // Not in use
  height:    number;
  weight:    number;
  stats:     Stat[];
}

export interface Stat {
  base_stat: number;
}
