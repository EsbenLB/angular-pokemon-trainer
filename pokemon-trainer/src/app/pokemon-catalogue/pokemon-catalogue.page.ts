import { Component, OnInit, PipeTransform } from "@angular/core";
import { Pokemons, Result } from "../models/pokemons.model";
import { PokemonsService } from "../services/pokemons.service";
import { TrainerService } from "../services/trainer.service";

@Component({
    selector: 'app-pokemons-catalogue',
    templateUrl: './pokemon-catalogue.page.html',
    styleUrls: ['./pokemon-catalogue.page.css']

})
// export class PokemonCataloguePage {}
export class PokemonCataloguePage implements OnInit  {
    constructor(private readonly pokemonService: PokemonsService, private readonly trainerServie: TrainerService){
    }
    // Expand UI: showMoreInfo about pokemon box
    private _showMoreInfo: boolean[] = []
    ngOnInit(): void{
      // Number of pokemon
      const limit = 200
      this.pokemonService.fetchPokemons(limit)
      // Add showMoreInfo bool for every pokemon
      this._showMoreInfo = [...Array(limit)].map(x => false);


    }

    get pokemons(): Pokemons{
        return this.pokemonService.pokemons()
    }
    public pokemon(id: number): Result{
      return this.pokemonService.pokemon(id)
    }
    // Toggle expand showMoreInfo
    public pokemonShowMore(i: number) {
      this._showMoreInfo[i] = !this._showMoreInfo[i];
    }
    public showMoreInfo(i: number): boolean{
      return this._showMoreInfo[i]
    }

    public onAdd(id: number) {
      // Already collected?
      if(this.pokemonService.pokemon(id).isCollected){
        return
      }
      let pokeUser ={
        "id": "id",
        "username": "username",
        "pokemon": [] as any
      }
      // Get user
      pokeUser = JSON.parse(localStorage.getItem("user") as string);
      // Add selected pokemon to user local stoarge
      pokeUser.pokemon.push(this.pokemonService.pokemon(id).name)
      // Collect pokemon
      this.pokemonService.pokemonCollected(id)
      // Add pokemons to user local-storage
      localStorage.setItem("user", JSON.stringify(pokeUser))
      // Update API
      this.trainerServie.updateTrainerPokemon(pokeUser.pokemon, 0, true)
    }
}
