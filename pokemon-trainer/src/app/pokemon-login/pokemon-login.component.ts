// logic file
import { Component, OnInit }  from '@angular/core'
import { NgForm }   from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { User } from '../models/pokemon.model';

// Decorators // tell role of this file -> component file
@Component({
    selector: 'app-pokemon-login',
    templateUrl: './pokemon-login.component.html',
    styleUrls: ['./pokemon-login.component.css']
})
export class PokemonloginComponent {

    constructor(private readonly loginService: LoginService,private router:Router){

    }


    // On login
    public onSubmit(createForm : NgForm): void{
        // Checks for user
        this.loginService.fetchUser(createForm.value.username);

        //console.log(createForm.value.username)
    }

    ngOnInit(): void{
        // Check if user is logged in

        if(localStorage.getItem("user") != null){
            // Navigate to trainer
            this.router.navigateByUrl("trainer")
        }
    }



}

