// api
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { User } from "../models/pokemon.model"


const link = "https://noroff-assignment-api-erlend.herokuapp.com/trainers"
const apiKey = "L9M22VKQ09XY6B4NPMRL4VWOBX2QN9SVQXO6O87H3LZR334OFTNV3AT43WR6IXK9"
const pokeApi = "https://pokeapi.co/api/v2/pokemon/"

@Injectable({
    providedIn: "root"
})

export class LoginService {
    private userN: User[] = [];


    private createHeaders(){
        return new HttpHeaders({
            "Content-Type": "application/json",
            "x-api-key": apiKey
        })
    }

    constructor(private readonly http: HttpClient, private router: Router){

    }


    // Fetches user data
    public fetchUser(username: string): void{
        this.http.get<User[]>(link+"?username="+username)
        .subscribe((userN: User[]) =>{
            this.userN = userN

            if(this.userN[0] == undefined){

                // Create user
                this.createUser(username).subscribe({
                    next:(response: any) =>{
                        // Log the user in

                        localStorage.setItem("user", JSON.stringify(response))
                        // Move user to page
                        this.router.navigateByUrl("trainer")
                    }
                })

            } else {

                // Save user to local storage
                localStorage.setItem("user", JSON.stringify(this.userN[0]))
                // Move user to page
                this.router.navigateByUrl("trainer")
            }


        }, (error: HttpErrorResponse) =>{

        })

    }

    // Function for creating user
    public createUser(username: string): Observable<any>{
        const user = {
            username,
            pokemon: []
        }
        const headers = this.createHeaders()
        return this.http.post(link,user,{
            headers
        })
    }


}
