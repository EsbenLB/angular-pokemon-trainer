import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Pokemon, Pokemons as PokemonList, Result } from '../models/pokemons.model'
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class PokemonsService{

    private _pokemonList: PokemonList = {count: 0,  next: "", previous: "", results:[]};

    private _error: string = '';

    private _url: string = 'https://pokeapi.co/api/v2/';
    private _urlPokePicture: string = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/";
    private _urlPokemon: string = "https://pokeapi.co/api/v2/pokemon/";


    // DI = Dependency Injection
    constructor(private readonly http: HttpClient){ }

    // fetch list of pokemon, then add picture_link, then fetch every pokemon in list
    public fetchPokemons(limit: number = 200): void{
      if(localStorage.getItem("pokemons") != null){

        this._pokemonList = JSON.parse(localStorage.getItem("pokemons") as string);

        return
    }
      // Angular does not user promises for http, uses Observable
      this.http.get<PokemonList>
      (this._url + "pokemon?limit="+ limit+ "&offset=0")
      .subscribe((pokemonList: PokemonList) => {
          this._pokemonList = pokemonList
          // for every pokemon in list
          for (let index = 0; index < this._pokemonList.results.length; index++) {
            // add picture url
            let pokeIndex: string = this._pokemonList.results[index].url.split("/pokemon/")[1].replace("/", "")
            this._pokemonList.results[index].pic = this._urlPokePicture + pokeIndex + ".png"
            this._pokemonList.results[index].isCollected = false
            // fetch pokemon by name
            this.fetchPokemon(this._pokemonList.results[index].name).subscribe({
              next:(response: any) =>{
                // reset pokemon array
                this._pokemonList.results[index].pokemon = []

                let poke ={
                  "id": response.id,
                  "height": response.height,
                  "weight": response.weight,
                  "stats": response.stats[0].base_stat
                }
                this._pokemonList.results[index].pokemon.push(poke)
                // Update pokemon "db" in localStorage
                localStorage.setItem("pokemons", JSON.stringify(this._pokemonList))
            }})
          }
          return this._pokemonList
        }
        )
    }
    // Get one pokemon by name
    public fetchPokemon(name: string): Observable<Pokemon>{
      return this.http.get<Pokemon>(this._urlPokemon + name)
    }
    // save/Update pokemons localstorage
    public savePokemons(){
      localStorage.setItem("pokemons", JSON.stringify(this._pokemonList))
    }
    // Get all pokemon
    public pokemons(): PokemonList{
        return this._pokemonList;
    }
    public pokemon(id: number): Result{
      return this._pokemonList.results[id];
    }
    // Collect pokemon
    public pokemonCollected(id: number) {
      this._pokemonList.results[id].isCollected = true;
      this.savePokemons()
    }

    // Uncollect pokemon
    // Alt e +1 i forhold til ka tallet e

    public pokemonUnCollected(id: number) {

      id =Math.floor(id);
      this._pokemonList.results[id-1].isCollected = false;
      this.savePokemons()

    }


    public error(): string{
        return this._error;
    }
}
