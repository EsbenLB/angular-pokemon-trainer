import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { PokemonsService } from "../services/pokemons.service";


const link = "https://noroff-assignment-api-erlend.herokuapp.com/trainers"
const apiKey = "L9M22VKQ09XY6B4NPMRL4VWOBX2QN9SVQXO6O87H3LZR334OFTNV3AT43WR6IXK9"
const pokeApi = "https://pokeapi.co/api/v2/pokemon/"

@Injectable({
    providedIn: "root"
})

export class TrainerService {


    constructor(private readonly http: HttpClient, private readonly pokemonsService: PokemonsService){

    }


    // Gets name from local storate
    private name = JSON.parse(localStorage.getItem("user") as string)

    // Function for updating pokemons in api
    public updateTrainerPokemon(pokelis: Array<string>,removed : number, collect: boolean){

          const pok:object = {
              id: this.name.id,
              username: this.name.username,
              pokemon: pokelis
          }


          const headers = {'X-API-Key': apiKey, 'Content-Type': 'application/json'}

          const body = {pokemon: pokelis}

          this.http.patch<any>(link+"/"+this.name.id,body,{headers})
          .subscribe({
              next:(response: any) =>{

                  this.pokemonsService.fetchPokemons(200)
                  if(collect){

                  } else{
                      // Setts pokemons status as uncollected
                      this.pokemonsService.pokemonUnCollected(removed)
                  }

                  localStorage.setItem("user", JSON.stringify(pok))

              }
          })
    }
}
