import { Component } from "@angular/core";
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
    selector: 'app-pokemons',
    templateUrl: './trainer.page.html'
})
export class TrainerPage {

    constructor(private trainerService: TrainerService){

    }


    // Pokemons from localstorage
    public pokeArray :object [] = []
    public pokList = {
        "result": [] as any
    }


    // get fill pokearray from localstorage on init
    ngOnInit(): void{

        let pokeUser ={
            "id": "id",
            "username": "username",
            "pokemon": []
        }

         pokeUser = JSON.parse(localStorage.getItem("user") as string)




        // Takes every pokemon and getts its data
        pokeUser.pokemon.forEach(pokeM => {
            const pokePush ={
                "id": "id",
                "name": "name",
                "src": "src"
            } 

            // Find data for pokemon

            const obj =this.getDataPokemon(pokeM)

            pokePush.name = obj.name
            pokePush.src = obj.pic

            // extract id from url
            let ets = ""+obj.pic
            pokePush.id = ets.substring(ets.lastIndexOf("/")+1, ets.lastIndexOf("."))
            
            
            // adds to list
            this.pokList.result.push(pokePush) 



        });

    }

    // Gets the pokemon data based on their name
    private getDataPokemon(pokename: string){
        
        let data = JSON.parse(localStorage.getItem("pokemons") as string)
        let obj = data.results.find((o: { name: string; }) => o.name ===pokename);
        return obj
    }

    // Remove pokemon from trainer
    public onDelete( name:string, id: number){

        // Get from local storage
        let localPokemon = JSON.parse(localStorage.getItem("user") as string).pokemon
        
        
        // Finding pokemon
        // Updating list
        localPokemon.splice(localPokemon.indexOf(name),1)

        // patch new list to api
        this.trainerService.updateTrainerPokemon(localPokemon, id, false)

        // Update localstorage on success

        
    }
}